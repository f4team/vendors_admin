import md5 from 'js-md5';
let Base64 = require('js-base64').Base64;
import service from '../config/axios_request';
import qs from "qs";


export const login = (userName, password) => {
    // password = md5(password)
    // let Authorization = Base64.encode(userName + ':' + password);
    return service({
        method: 'post',
        url: '/login',
        // headers: {
        //     "Authorization": "Basic " + Authorization,
        // },
        data:{
            userName:userName,
            password:password
        }
    }).then(res => {
        return new Promise(function(resolve,reject){
            resolve(res);
            // reject(res);
        })
    })
}

export const loginBytoken = (refresh_token) => {
    return service({
        method: 'post',
        url: '/api/mc/v1/auth/refresh_token',
        params: {
            refresh_token: refresh_token
        }
    })
}

export const logout = () => {
    return service({
        method: 'GET',
        url: '/loginOut',
    })
}

export const checkIsLoginApi = () => {
    return service({
        method: 'get',
        url: '/api/mc/v1/permissions',
    })
}
/**获取权限列表 */
export const getPermissionsData = (pagePer) => {
    let par = {};
    if(pagePer){
        par = {
            page:pagePer.page,
            size:pagePer.size,
        }
    }else{
        par = {
            page:0,
            size:100000000000,
        }
    }
    return service({
        method: 'get',
        url: '/api/mc/v1/permissions',
        params: par
    })
}
/**获取权限详情 */
export const getPermissionsDetail = (id) => {
    return service({
        method: 'get',
        url: '/api/mc/v1/permissions/'+id,
    })
}
/**添加权限 */
export const addPermissions = (permission) => {
    let data = {
        description:permission.description,
        resource:permission.resource,
    }
    if(permission.id){
        data.id = permission.id;
    }
    if(permission.parentId){
        data.parentId = permission.parentId;
    }
    return service({
        method: 'post',
        url:'/api/mc/v1/permissions',
        data: JSON.stringify(data),
        headers: {
            "content-type": 'application/json'
        }
    })
}
/**删除权限 */
export const deletPermissions = (id) => {
    return service({
        method: 'delete',
        url: '/api/mc/v1/permissions/'+id,
    })
}

/**获取角色列表 */
export const getRoleData = (pagePer) => {
    let par = {};
    if(pagePer){
        par = {
            page:pagePer.page,
            size:pagePer.size,
        }
    }else{
        par = {
            page:0,
            size:100000000000,
        }
    }
    return service({
        method: 'get',
        url: '/api/mc/v1/roles',
        params: par
    })
}
/**获取角色详情 */
export const getRoleDetail = (id) => {
    return service({
        method: 'get',
        url: '/api/mc/v1/roles/'+id,
    })
}
/**添加角色 */
export const addRole = (role) => {
    let data = {
        description:role.description,
        role:role.role,
        permissions:role.permissions
    }
    if(role.id){
        data.id = role.id;
    }
    return service({
        method: 'post',
        url:'/api/mc/v1/roles',
        data: JSON.stringify(data),
        headers: {
            "content-type": 'application/json'
        }
    })
}
/**删除角色 */
export const deletRole = (id) => {
    return service({
        method: 'delete',
        url: '/api/mc/v1/roles/'+id,
    })
}

/**获取账号列表 */
export const getUserData = (pagePer) => {
    return service({
        method: 'get',
        url: '/api/mc/v1/users',
        params: {
            keyword:pagePer.keyword,
            page:pagePer.page,
            size:pagePer.size,
        }
    })
}
/**获取账号详情 */
export const getUserDetail = (id) => {
    return service({
        method: 'get',
        url: '/api/mc/v1/users/'+id,
    })
}
/**添加、编辑账户 */
export const addUser = (user) => {
    let data = {
        username:user.username,
        nickname:user.nickname,
        roles:user.roles,
        enterpriseId:user.enterpriseId,
    }
    if(user.id){
        data.id = user.id;
    }
    if(user.password){
        data.password = md5(user.password);
    }
    return service({
        method: 'post',
        url:'/api/mc/v1/users',
        data: JSON.stringify(data),
        headers: {
            "content-type": 'application/json'
        }
    })
}
/**删除账户 */
export const deletUser = (id) => {
    return service({
        method: 'delete',
        url: '/api/mc/v1/users/'+id,
    })
}
/**获取当前账号信息 */
export const getCurrentUser = (id) => {
    return service({
        method: 'get',
        url: '/api/mc/v1/users/current',
    })
}

/**获取企业列表 */
export const getCompanyData = (pagePer) => {
    let par = {};
    if(pagePer){
        par = {
            keyword:pagePer.keyword,
            page:pagePer.page,
            size:pagePer.size,
        }
    }else{
        par = {
            page:0,
            size:100000000000,
        }
    }
    return service({
        method: 'get',
        url: '/api/mc/v1/enterprises',
        params: par,
    })
}
/**获取企业详情 */
export const getCompanyDetail = (id) => {
    return service({
        method: 'get',
        url: '/api/mc/v1/enterprises/'+id,
    })
}
/**添加、编辑企业 */
export const addCompany = (company) => {
    let data = {
        name:company.name,
        description:company.description,
        email:company.email,
        contacts:company.contacts,
        address:company.address,
        longitude:company.longitude,
        latitude:company.latitude,
        telephones:company.telephones,
    }
    if(company.id){
        data.id = company.id;
    }
    return service({
        method: 'post',
        url:'/api/mc/v1/enterprises',
        data: JSON.stringify(data),
        headers: {
            "content-type": 'application/json'
        }
    })
}
/**删除企业 */
export const deletCompany = (id) => {
    return service({
        method: 'delete',
        url: '/api/mc/v1/enterprises/'+id,
    })
}
/**获取当前账号的企业信息 */
export const getPersonCompany = (id) => {
    return service({
        method: 'get',
        params:{
            id: id
        },
        url: 'api/mc/v1/enterprises/current',
    })
}

/**获取分类列表 */
export const getProductCateData = (pagePer) => {
    let par = {};
    if(pagePer){
        par = {
            keyword:pagePer.keyword,
            page:pagePer.page,
            size:pagePer.size,
        }
    }else{
        par = {
            page:0,
            size:100000000000,
        }
    }
    return service({
        method: 'get',
        url: '/getProductTypeListHome',
        params: par
    })
}
/**获取分类详情 */
export const getProductCateDetail = (id) => {
    return service({
        method: 'get',
        url: '/api/mc/v1/categories/'+id,
    })
}
/**添加分类 */
export const addProductCate = (productCate) => {
    let data = {
        typeName:productCate.typeName,
        image: productCate.image.raw ? productCate.image.raw : '',
    }
    return service({
        method: 'post',
        url:'/addProductType',
        data: data,
    })
}
/**编辑分类 */
export const editProductCate = (productCate) => {
    let data = {
        typeName:productCate.typeName,
        typeID: productCate.id,
        image: productCate.image.raw ? productCate.image.raw : '',
    }
    return service({
        method: 'post',
        url:'/updateProductType',
        data: data,
    })
}
/**删除分类 */
export const deletProductCate = (id) => {
    return service({
        method: 'post',
        url: '/deleteProductType',
        data:{
            typeID: id
        }
    })
}


/**获取商品列表 */
export const getProductData = (pagePer) => {
    return service({
        method: 'get',
        url: '/getProductList',
        params: {
            keyword:pagePer.keyword,
            productType:pagePer.categoryId,
            page:pagePer.page,
            size:pagePer.size,
        }
    })
}
/**获取商品详情 */
export const getProductDetail = (id) => {
    return service({
        method: 'get',
        url: '/getProduct?productID='+id,
    })
}
/**添加、编辑商品 */
export const addProduct = (product) => {
    let data = {
        code:product.code,
        name:product.name,
        categoryId:product.categoryId,
        approval:product.approval,
        attributes:product.attributes,
        images:product.images,
        detailImages:product.detailImages,
    }
    if(product.id){
        data.id = product.id;
    }
    return service({
        method: 'post',
        url:'/api/mc/v1/products',
        data: JSON.stringify(data),
        headers: {
            "content-type": 'application/json'
        }
    })
}
/**修改商品显示状态 */
export const editProductApproval = (product) => {
    let data = {
        id:product.id,
        approval:product.approval,
    }
    return service({
        method: 'patch',
        url: '/api/mc/v1/products',
        data:JSON.stringify(data),
        headers: {
            "content-type": 'application/json'
        }
    })
}
/**删除商品 */
export const deletProduct = (id) => {
    return service({
        method: 'delete',
        url: '/api/mc/v1/products/'+id,
    })
}
/**恢复商品删除状态 */
export const editProductEnable = (product) => {
    let data = {
        id:product.id,
        enabled:product.enabled,
    }
    return service({
        method: 'patch',
        url: '/api/mc/v1/products',
        data:JSON.stringify(data),
        headers: {
            "content-type": 'application/json'
        }
    })
}

// 获取产品图片列表
export const getProductImageData = (id) => {
    let par = {
        productID:id
    };
    return service({
        method: 'get',
        url: '/getProductDetail',
        params: par
    })
}
// 添加产品图片
export const addProductImageData = (image,id) => {
    let data = {
        productID:id,
        image:image
    };
    return service({
        method: 'post',
        url: '/addProductDetail',
        data: data
    })
}
// 删除产品图片
export const deleteProductImageData = (imgUrl) => {
    let data = {
        imgUrl:imgUrl,
    };
    return service({
        method: 'post',
        url: '/deleteProductDetailPhoto',
        data: data
    })
}

/**获取图片分类列表 */
export const getImgCateData = (pagePer) => {
    let par = {};
    if(pagePer){
        par = {
            page:pagePer.page,
            size:pagePer.size,
        }
    }else{
        par = {
            page:0,
            size:100000000000,
        }
    }
    return service({
        method: 'get',
        url: '/api/mc/v1/images/groups',
        params: par,
    })
}
/**获取图片分类详情 */
export const getImgCateDetail = (id) => {
    return service({
        method: 'get',
        url: '/api/mc/v1/images/groups/'+id,
    })
}
/**添加、编辑图片分类 */
export const addImgCate = (productCate) => {
    let data = {
        name:productCate.name,
    }
    if(productCate.id){
        data.id = productCate.id;
    }
    return service({
        method: 'post',
        url:'/api/mc/v1/images/groups',
        data: JSON.stringify(data),
        headers: {
            "content-type": 'application/json'
        }
    })
}
/**删除图片分类 */
export const deletImgCate = (id) => {
    return service({
        method: 'delete',
        url: '/api/mc/v1/images/groups/'+id,
    })
}

/**获取图片列表 */
export const getImageData = (pagePer) => {
    let par = {};
    if(pagePer){
        par = {
            keyword:pagePer.keyword,
            groupId:pagePer.groupId,
            page:pagePer.page,
            size:pagePer.size,
        }
    }else{
        par = {
            page:0,
            size:100000000000,
        }
    }
    return service({
        method: 'get',
        url: '/api/mc/v1/images',
        params: par
    })
}
/**添加图片 */
export const addImageCate = (image) => {
    let data = {
        file:image.file,
    }
    return service({
        method: 'post',
        url:'/api/mc/v1/images',
        data: JSON.stringify(data),
        headers: {
            "content-type": 'application/json'
        }
    })
}
/**修改图片 */
export const editImageCate = (image) => {
    return service({
        method: 'put',
        url:'/api/mc/v1/images',
        data: JSON.stringify(image),
        headers: {
            "content-type": 'application/json'
        }
    })
}
/**删除图片 */
export const deletImageCate = (ids) => {
    let paramsStr = '?id=' + ids.join('&id=');
    return service({
        method: 'delete',
        url: '/api/mc/v1/images' + paramsStr,
    })
}

/**批量上传图片 */
export const pushImg = (file) => {
    return service({
        method:'post',
        url:'/cms/api/mc/v1/images',
        data: JSON.stringify(file),
        headers: {
            "content-type": 'application/json'
        }
    })
}

/**获取faq列表 */
export const getFaqData = (pagePer) => {
    return service({
        method: 'get',
        url: '/api/mc/v1/faqs',
        params: {
            keyword:pagePer.keyword,
            page:pagePer.page,
            size:pagePer.size,
        }
    })
}
/**获取faq详情 */
export const getFaqDetail = (id) => {
    return service({
        method: 'get',
        url: '/api/mc/v1/faqs/'+id,
    })
}
/**添加、编辑faq */
export const addFaq = (faq) => {
    let data = {
        question:faq.question,
        answer:faq.answer,
    }
    if(faq.id){
        data.id = faq.id;
    }
    return service({
        method: 'post',
        url:'/api/mc/v1/faqs',
        data: JSON.stringify(data),
        headers: {
            "content-type": 'application/json'
        }
    })
}
/**删除faq */
export const deletFaq = (id) => {
    return service({
        method: 'delete',
        url: '/api/mc/v1/faqs/'+id,
    })
}

/**获取案例列表 */
export const getProjectData = (pagePer) => {
    let par = {};
    if(pagePer){
        par = {
            keyword:pagePer.keyword,
            approval:pagePer.approval ? pagePer.approval : '',
            page:pagePer.page,
            size:pagePer.size,
        }
    }else{
        par = {
            page:0,
            size:100000000000,
        }
    }
    return service({
        method: 'get',
        url: '/api/mc/v1/projects',
        params: par
    })
}
/**获取案例详情 */
export const getProjectDetail = (id) => {
    return service({
        method: 'get',
        url: '/api/mc/v1/projects/'+id,
    })
}
/**添加、编辑案例 */
export const addProject = (project) => {
    let data = {
        name:project.name,
        description:project.description,
        approval:project.approval,
        images:project.images,
    }
    if(project.id){
        data.id = project.id;
    }
    return service({
        method: 'post',
        url:'/api/mc/v1/projects',
        data: JSON.stringify(data),
        headers: {
            "content-type": 'application/json'
        }
    })
}
/**修改案例显示状态 */
export const editProjectApproval = (project) => {
    let data = {
        id:project.id,
        approval:project.approval,
    }
    return service({
        method: 'patch',
        url: '/api/mc/v1/projects',
        data:JSON.stringify(data),
        headers: {
            "content-type": 'application/json'
        }
    })
}
/**删除案例 */
export const deletProject = (id) => {
    return service({
        method: 'delete',
        url: '/api/mc/v1/projects/'+id,
    })
}

/**获取留言列表 */
export const getMessagesData = (pagePer) => {
    return service({
        method: 'get',
        url: '/api/mc/v1/messages',
        params: {
            page:pagePer.page,
            size:pagePer.size,
        }
    })
}
/**获取留言详情 */
export const getMessagesDetail = (id) => {
    return service({
        method: 'get',
        url: '/api/mc/v1/messages/'+id,
    })
}
