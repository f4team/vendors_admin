import axios from "axios";
import qs from "qs";
import {
    apiBaseUrl
} from './env'
import {
    Loading
} from 'element-ui';
import {
    getToken,refreshToken,deletTokenInStore
} from './mUtils'
import {
    errorMsgMap
} from './msg'
import { Message } from 'element-ui';

/****** 创建axios实例 ******/
const service = axios.create({
    baseURL: apiBaseUrl, // api的base_url
    timeout: 5000 // 请求超时时间
});
let loadingInstance = '';//全屏loading


/****** request拦截器==>对请求参数做处理 ******/
service.interceptors.request.use(config => {
    loadingInstance = Loading.service({ fullscreen: true });
    // config.method === 'post' ?
    //     config.data = qs.stringify({
    //         ...config.data
    //     }) :
    //     config.params = {
    //         ...config.params
    //     };
    if(config.method === 'post'){
        let form = new FormData()
        let keys = Object.keys(config.data)
        keys.forEach(key => {
          form.append(key, config.data[key])
        })
        config.data = form;
        config.headers['Content-Type'] = 'multipart/form-data'
    }
    // config.headers['Content-Type'] = 'application/x-www-form-urlencoded';
    // let token = getToken()
    // if(token && !config.headers['Authorization']){
    //     config.headers['Authorization'] = 'Bearer ' + token.access_token;
    // }
    return config;
}, error => { //请求错误处理
    loadingInstance = Loading.service({ fullscreen: true });
    Promise.reject(error)
})
/****
 * ** respone拦截器==>对响应做处理 ******/
service.interceptors.response.use(
    response => { //成功请求到数据
        loadingInstance.close();
        if(response.data.code == '1'){
            return Promise.reject({
                message: response.data.data.msg
            })
        }
        //这里根据后端提供的数据进行对应的处理
        return response.data;
    },
    error => { //响应错误处理
        loadingInstance.close();

        // /**令牌无效 */
        let rep = /\#\/login/;
        let isLoginPage = rep.test(window.location.href);
        // /**令牌过期 */
        // if(error.response.status === 401 && !isLoginPage){
        //     refreshToken();
        // }
        /**插入错误信息 */
        if((error.response.status === 400 && error.response.data.error) || (error.response.status === 401 && isLoginPage)){
            let requestUrl = error.response.config.url;
            let requestUrlName = requestUrl.split(apiBaseUrl)[1];
            let requestMethod = error.response.config.method;
            error.response.data.error_description = errorMsgMap.get(requestUrlName+'_'+requestMethod) ? 
                errorMsgMap.get(requestUrlName+'_'+requestMethod)[error.response.data.error] : error.response.data.error_description;
        }
        // /**无权限 */
        // if(error.response.status === 403){
        //     Message.error('该账号无此操作的权限');
        // }
        // /**系统错误 */
        // if(error.response.status === 500){
        //     Message.error('系统错误，请联系管理员');
        // }

        return Promise.reject(error)
    }
)

export default service;